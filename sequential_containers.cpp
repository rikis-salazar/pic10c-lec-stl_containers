// Program that illustrates some features and/or member functions associated to
// Sequential containers (e.g., vector, deque, list).

#include <iostream>
#include <vector>
#include <deque>
#include <list>

// Declaration of helper functions:
template<typename Container>
void print_to_console( Container c );


// main routine
int main(){
    using namespace std; // <-- Misplaced?!

    // Let us start with a simple array...
    const int SIZE = 5;
    int a[] = {1,3,6,8,0};

    // ... as well as a wish list.
    // print_container(&a[0],&a[SIZE]);
    // Oh, well... let us follow the usual path

    cout << "Contents of the array (via for loop):\n";
    for ( int i = 0 ; i < SIZE ; ++i )
        cout << a[i] << ' ';
    cout << '\n';

    cout << "Contents of the array (via for loop + pointer arithmetic):\n";
    for ( int* p = &a[0] ; p != &a[SIZE] ; ++p )
        cout << *p << ' ';
    cout << '\n';

    // Enough with arrays! Let us initialize other containers. For example,
    // according to your textbook, several different constructors are
    // available. Some of these are:
    //     container()
    //     container( size )
    //     container( size, value )
    //
    // as well as 
    //     container( RANGE )    
    //
    //  where a RANGE consists roughly speaking of "a collection of values"


    // If container is replaced by `vector<int>` we should expect the 
    // statement below to work
    //     vector<int> v();
    //
    // however, the compiler doesn't quite see it this way.
    // As a workaround, we can try
    vector<int> v = vector<int>();   // as a fancy way to write `vector<int> v`


    // Constructing via
    //     container( size )
    deque<int> d(SIZE);    // empty deque d={}  // <-- Can you spot the mistake???
    // Let us keep on adding to the wish list...  Wouldn't it be nice to have a
    // way to 'overload' the assignment operator, so that statements like the
    // ones below would perform the proper initialization?
    //     d = v;   // <-- the contents of v are passed onto d
    //     d = a;   // <-- the contents of a are passed onto d
    //
    // Unfortunately, this is not possible. However, we'll see later that we
    // can simply `copy` from one container into another.


    // Constructing via
    //     container( size, value )
    list<int> l(SIZE,7);             // non-empty list l={7,7,7,7,7}


    // Let us now ...
    // ... display the contents of the containers using loops
    //   * the old fashion way 
    cout << "\nContents of [some of] the containers (the old fashion way)\n";
    cout << "Contents of the array (via for loop):\n";
    for ( int i = 0 ; i < SIZE ; ++i )
        cout << a[i] << ' ';
    cout << '\n';

    cout << "Contents of the vector (via for loop):\n";
    for ( int i = 0 ; i < v.size() ; ++i )
        cout << v[i] << ' ';
    cout << '\n';

    cout << "Contents of the deque (via for loop):\n";
    for ( int i = 0 ; i < d.size() ; ++i )
        cout << d[i] << ' ';
    cout << '\n';

    // As we saw during lectures, the previous approach doesn't work for lists.
    // This is because the member function `operator[](int)` is not defined for
    // lists. To verify this, try playing around with the following block
    /**
        cout << "Contents of the list (via for loop):\n";
        for ( int i = 0 ; i < l.size() ; ++i )
            cout << l[i] << ' ';
        cout << '\n';
    */


    // Let us now ...
    // ... display the contents of the containers using loops
    //   * the new 'iterated' way.
    cout << "\nContents of the containers (the new iterated way)\n";

    // We take a clue from upstairs were we used the statements
    //     cout << "Contents of the array:\n";
    //     for ( int* p = &a[0] ; p != &a[SIZE] ; ++p )
    //          cout << *p << ' ';
    //     cout << '\n';
    //
    // Notice that in this case, `&a[0]`, and `&a[SIZE]` represent the
    // locations of the beginning and end of the array. So, if a container is
    // able to report these locations, we should be able to use the same type
    // of loop.  For a vector we can try
    cout << "Contents of the vector:\n";
    for ( vector<int>::iterator p = v.begin() ; p != v.end() ; ++p )
        cout << *p << ' ';
    cout << '\n';

    // Next, notice that for a container `c`, it is a little bit painful having
    // to write
    //     container<TYPE>::iterator p = c.begin() 
    // inside the for loop. Luckily, when using the `C++11` standards, one can 
    // ask  the compiler to determine the right type. After all, since the compiler
    // knows what is the `c.begin()` function returning, in principle it should 
    // be able to also determine the appropriate type for the "variable" `p`.
    // The keyword `auto` kindly asks for the compiler to figure out the correct
    // type for `p`.
    cout << "Contents of the deque:\n";
    for ( auto p = d.begin() ; p != d.end() ; ++p )
        cout << *p << ' ';
    cout << '\n';

    // As a nice consequence if the use of `auto`, our life is actually
    // simplified. We can use the same "trick" with the list, and even with the
    // array, provided that we point out where it begins and where it ends.
    cout << "Contents of the list:\n";
    for ( auto p = l.begin() ; p != l.end() ; ++p )
        cout << *p << ' ';
    cout << '\n';

    cout << "Copying the array into the deque:\n";
    for ( auto p = a ; p != a + SIZE ; ++p ){
        cout << *p << " enters the vector.\n"  ;
        v.push_back(*p);
    }
    cout << '\n';


    // There is one last set of constructors that we want to study; they take a
    // "RANGE" of elements, just like the previous for loops.
    //      * A deque built from a piece of an array
    deque<int> d_inc_a( a+1 , a+4 );

    //  A deque built from a vector traveled backwards.
    deque<int> d_rev_v( v.rbegin() , v.rend() );

    //  A deque built from a list
    deque<int> d_l( l.begin() , l.end() );


    // Go ahead, use the following lines to display the contents of these newly
    // created containers.
    auto c = d_inc_a;
    // auto c = d_rev_v;
    // auto c = d_l;
    cout << "Contents of ___FILL_IN_THE_BLANK___:\n";
    for ( auto p = c.begin() ; p != c.end() ; ++p )
        cout << *p << ' ';
    cout << '\n';


    // To conclude this [rather long] demo, let us try to improve the way we
    // output things to the console. A little bit of thought, reveals that
    // since we are [mostly] working with containers it actually makes sense to
    // use a template function, say `print_to_console`, to display the values
    // stored in these containers. If you scroll over to the beginning of these
    // file, you will find the declaration
    //
    //     template<typename Container>
    //     void print_to_console( Container c );
    //
    // This function is a simple call to an iterated for loop, like the many others
    // we have seen before. The actual implementation can be found after the main 
    // routine.
    cout << "\nContainers via 'print_to_console'\n";
    cout << "print_to_console(v): ";
    print_to_console(v);
    cout << "print_to_console(d): ";
    print_to_console(d);
    cout << "print_to_console(l): ";
    print_to_console(l);
    cout << "print_to_console(d_inc_a): ";
    print_to_console(d_inc_a);
    cout << "print_to_console(d_rev_v): ";
    print_to_console(d_rev_v);
    cout << "print_to_console(d_l): ";
    print_to_console(d_l);

    
    // The function 'print_to_console' does indeed represent an improvement
    // over our use of the iterated for loop. However, notice that if is not
    // very flexible.  Indeed, the call
    //     print_to_console(a);   // <-- Oops!

    // fails because there are no such functions as 'a.begin()' nor 'a.end()'.
    // Moreover, we cannot use it 
    //     * to display a backwards list, nor
    //     * to display only some elements of a deque. 

    // So, what do we do? Do we simply give up? Of course not. However though,
    // since The Los Angeles Dodgers are about to play, I cannot keep working
    // on this "lesson". I can nevertheless, point out that the last container
    // we tested here, does provide the missing functionality that we seek yo
    // implement.  Wouldn't it be nice if we could simply write:
    print_container( a , a + SIZE );                  // full array
    print_container( v.rbegin() , v.rbegin() + 3 );   // last three in vector (backwards)
    print_container( l.begin() , l.end() );           // full list
    print_container( d_l.rbegin() , d_l.rend() );     // __FILL_IN_THE_BLANK__

    // Well... At some point we will code a 'generic' functions that does just
    // that.  In the meantime you are encouraged to provide your own solution.

    return 0;
}



// Definition of helper functions
template<typename Container>
void print_to_console( Container c ) {
    for( auto iter = c.begin() ; iter != c.end() ; ++iter ){
        // cout << *iter << ' ';         // <-- Oops!
        std::cout << *iter << ' ';
    }
    std::cout << '\n';
    return;
}
